
Chcemy przyłączyć do siebie branche "master" oraz "iss53". Zauważmy kluczowy fakt że:
- w momencie rozpoczynania pracy odłączyliśmy się od branchy master w commicie "C2"
- podczas pracy w swojej gałęzi "iss53" dodane zostały commity do gałęzi master "C4"

Czyli zestaw zmian z gałęzi "iss53", na której obecnie jesteś, nie jest bezpośrednim potomkiem gałęzi, którą scalasz.

Tutaj mamy doczynienia ze złączeniem trójczłonowym (ang. three-way merge)

Jego wynikiem jest stworzenie tzw. merge-commit-a ("C6") przez git-a, który zawiera najnowsze zmiany z ostatnich commitów w branchach oraz wspólnego przodka


# przed

![[basic-merging-1.svg]]
# po
![[basic-merging-2.svg]]