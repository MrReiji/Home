Origin - domyślna nazwa repozytorium zdalnego

Git - może pracować bez dostępu do internetu, natomiast jeśli współpracujemy z innymi osobami chcemy mieć miejsce do którego wypychamy nasze zmiany. Sytuacja ta dla 2 osób została pokazana poniżej.


![[distributed.svg]]
Każdy z węzłów ma swoją bazę wersji, którą trzeba jawnie synchronizować.
O ile commity są takie same w każdej z baz danych, to gałęzie występują zdalne oraz lokalne.

Nazwy gałęzi zdalnych powstają wg schematu: (nazwa zdalnego repozytorium) / (nazwa gałęzi)

Po wykonaniu komendy git clone, git automatycznie pobierze repozytorium, nazwie je origin, przełączy się na domyślną lokalną branch (np. master / dev) i będzie śledził zdalne repozytorium pod nazwą "origin/(master/dev)"

"origin" jest domyślną nazwą zdalnego repo, podobnie jak branch "master"/"main" `git clone -o booyah` nazwie repo zdalne "booyah" 

Gdy mamy repozytorium zdalne musimy zadbać aby ono był synchronizowane. Patrząc na poniższą sytuację inny członek zespołu wprowadził zmiany na repozytorium zdalne, a na naszym komputerze nie pobraliśmy zmian, a wprowadziliśmy swoje już. Więc zostanie stworzony [[merge]], bo różne sha commitów.

Jeśli nie dodalibyśmy commitów, to po prostu pobralibyśmy przychodzące commity komendą `git pull`
Natomiast jak chcemy wysłać swoje zmiany używamy komendę `git push`


![[remote-branches-2.svg]]

Przy klonowaniu, oraz przy przełączaniu się na gałęzie komendą `git checkout -b origin/serverfix` git pod spodem ustala śledzenie gałęzi "origin/serverfix" z "serverfix"

```bash
Branch serverfix set up to track remote branch serverfix from origin.
Switched to a new branch 'serverfix'
```

Śledzenie jest istotne aby git widział na jaką zdalną gałąź wysłać zmiany lub z niej pobrać

Rozszerzoną wersją tej komendy jest zapis
```bash
$ git checkout -b sf origin/serverfix
Branch sf set up to track remote branch serverfix from origin.
Switched to a new branch 'sf'
```

Teraz, Twoja gałąź lokalna "sf" będzie automatycznie pobierać zmiany z "origin/servefix".
