Istnieje kilka strategi tworzenia [[commit]]-ów na różnych [[branch]]-ach.

Ogólnie można wyróżnić tzw gałęzie długo żyjące, produkcyjne takie jak m.in: dev, master, main, prod.
Gałęzie które powstają tylko na potrzeby zrealizowania danego task-a to gałęzie krótko żyjące, feature branch

## student flow

Jeśli pracujemy sami nad projektem dodawanie [[commit]]-ów do jednej gałęzi zadziała, natomiast jeśli pracujemy nad projektem te podejście nazwę autorsko "student" model. 

Wyobraźmy sobie sytuację że robimy commita, na wspólną gałąź. W tym samym czasie kilka innych osób też pracuje. Po zrobieniu naszego commit-a, i wypchnięciu zmian na zdalne repozytorium. Inni w momencie wypchnięcia zmian na repozytorium zdalne będą musieli pobrać nasze zmiany. Problem w tym że dowiedzą się o tym inni w momencie próby wypchania swoich zmian. A w przypadku podjęcia tej próby przez ostatnią osobę która nie pobierała zmian, repozytorium będzie "dosyć" nieaktualne

## GitHub flow

Flow te zawiera dwa typy gałęzi
- 1 długo żyjąca, zawierająca aktualną wersje aplikacji. Z tej gałęzi deweloperzy się odłączają i do niej przyłączają zmiany. Wersja aplikacji z tej gałęzi jest zawsze gotowa do wdrożenia
- robocze / feature branch. Rozwijanemu zadaniu przez dewelopera odpowiada gałąź robocza, do której regularnie wdraża swoje zmiany

Zazwyczaj ci/cd wdraża zmiany przy powstaniu [[pull request]] / [[merge request]] na środowisko testowe

Problem z GitHub powstaje jak dwie branch-e wprowadzają sprzeczne zmiany
## GitLab flow

Jest to "lżejsza: alternatywa GitFlow
Podobnie jak w GitHub gałęzie robocze powinny odzwierciedlać zadania. Natomiast pojawia się większa liczba gałęzi stabilnych. Przynajmniej:
- domyślna deweloperska z której deweloperzy domyślnie rozwijają funkcjonalności
- produkcyjna / master na której znajduje się wersja produkcyjna stabilna

W przypadku pojawienia się błędu na produkcji, które wymagają natychmiastowej poprawy tworzone się tzw. gałęzie hotfix, które wychodzą z produkcji i wchodzą do produkcji.

Natomiast błędy nie krytyczne, lub usprawnienia aplikacji standardowo wychodzą z deweloperskiej gałęzi i do niej są przyłączane.

W tym przypadku zmniejszany jest problem, że dwie gałęzie wprowadzają sprzeczne zmiany, bo jeśli są one wprowadzane do deweloperskiej gałęzi to nie są wdrażane na produkcji

Jeśli potrzebujemy utrzymywać kilka wersji aplikacji, to GitLab flow również znajdzie tu zastosowanie. Gałęzie produkcyjne będą będą miały nazwę odzwierciedlające wersje aplikacji, a [[commit]]-y np. z poprawką bezpieczeństwa, będą wprowadzane do wspomnianych gałęzi osobno.

# Git flow

Jest to najbardziej skomplikowany model. Jedną z wyróżniających go cech są tzw. gałęzie "release candidate" (RC).
Gałęzie te zawierają zmiany z produkcji oraz dev-a w momencie tzw. "feature freeze" (FF). FF to moment kiedy testerzy testują wersję aplikacji, która trafiła do RC-ka, a cała uwaga deweloperów jest skierowana na poprawki znalezionych błędów.

See also: https://docs.gitlab.co.jp/ee/topics/gitlab_flow.html

# Jaki flow wybrać

To zależy

Git flow, na projekt studencki w grupie raczej będzie overkill-em, ze względu na poziom skomplikowania. Natomiast GitHub flow dla monolitycznych aplikacji może być źródłem błędów. Więc zależy to od poziomu skomplikowania projektu. 

Możemy pomyśleć o GitHub flow przy wydawaniu prototypu aplikacji, przechodząc po pozytywnym przyjęciu prototypu przez klienta na model GitLab, a później też w Git flow

Na dzień pisania tych informacji, mały projekt / mikroserwis w projektach webowych jest tworzony w GitHub  flow, a bardziej rozbudowane aplikacje np. backend w GitLab flow
