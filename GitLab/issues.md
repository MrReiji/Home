W GitLab-ie istnieje mechanizm issues. Do issue może być dodane kilka task-ó, jednakże na przykładzie aplikacji webowej nie było do tej pory potrzeby rozdrabniania się w taski.

Issue:
- Zawiera tytuł oraz opis rzeczy, którą należy zrealizować.
- Może być przyblokowany przez inne issue, nawet z innego projektu, może być tez powiązany z innym
- Posiada miejsce w którym jest zapisana jest jego historia. 
- Komentarze są dobrym miejscem gdy opracowywany jest plan danej funkcjonalności, rozmowy itp
- Może wiązać ze sobą [[merge request]]-y
	- Aby powiązać ze sobą MR-a oraz issue, należy w opisie MR-a dodać w opisie wzmiankę o issue `#1`

W komentarzach zapis `#1` oznacza że odwołujemy się do issue o numerze 1, natomiast zapis `!1` oznacza odwołanie się do [[merge request]]-a o numerze 1

