Wyobraźmy sobie sytuację w której to mamy plik Readme, który w momencie rozgałęzień ma poniższą postać

```markdown
# Testowe repo
Jakiś piękny opis dodaje
 
# Sekcja 1

todo

# Sekcja 2

todo
```


Następnie, powstały 3 gałęzie w których to każdy commit (jeden) w gałęzi modyfikuje kolejno:
- branch `section1` sekcje 1
- branch `section1-update` sekcje 1
- branch `section2` sekcje 2

Po stworzeniu wspomnianych gałęzi historia commmitów przyjmuje poniższą postać

![[merge-conflicts-start.png]]

```mermaid
%%{init: { 'gitGraph': { 'mainBranchName': 'master'}} }%%
gitGraph
  commit id: "Initial commit"
  commit id: "Added title"
  commit id: "Added some text"
  commit id: "Added sections placeholder"
  branch section1
  commit id: "Added section 1 text"
  checkout master
  branch section2
  commit id: "added section 2"
  checkout master
  branch section1-updated
  commit id: "Added truly generated lorem ipsum to section 1"
```

![[merge without conflicts - fast forward]]

![[merge without conflicts - recursive]]



![[merge with conflicts]]