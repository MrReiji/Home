```mermaid
    gitGraph
	   commit id: "Initial commit"
       commit id: "Commit Alpha"
       commit id: "Commit Beta"
       commit id: "Commit Gamma"

```



Gałąź w Gicie jest po prostu lekkim, przesuwalnym wskaźnikiem na któryś z owych zestawów
zmian. Domyślna nazwa gałęzi Gita to master/main.

Kiedy zatwierdzasz pierwsze zmiany, otrzymujesz
gałąź master, która wskazuje na ostatni zatwierdzony przez Ciebie zestaw. Z każdym
zatwierdzeniem automatycznie przesuwa się ona do przodu.

Skąd Git wie, na której gałęzi się aktualnie znajdujesz? Utrzymuje on specjalny wskaźnik o nazwie
HEAD.

## Branch master oraz iss53

![[basic-branching-2.svg]]

## Branch master oraz iss53 po zmianach

![[basic-branching-6.svg]]

```mermaid
%%{init: { 'gitGraph': { 'mainBranchName': 'master'}} }%%
    gitGraph
       commit id: "C0"
       commit id: "C1"
       commit id: "C2"
       branch iss53
       commit id: "C3"
       checkout master
       commit id: "C4"
       checkout iss53
       commit id: "C5"

```