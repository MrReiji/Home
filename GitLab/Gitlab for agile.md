GitLab poza podstawową funkcjonalnością, jaką jest repozytorium zdalne, posiada więcej narzędzi.

Jednym z nich jest zestaw narzędzi przydatnych dla Agile. Na podobieństwo do Jira, Asana, Trello.
Na potrzeby aplikacji internetowej, funkcjonalności te są wystarczające i nie jest potrzebnie dodatkowe narzędzie typu jira

![[The-GitLab-Flow-2023-feedback-loops.png]]


See more: https://about.gitlab.com/blog/2023/07/27/gitlab-flow-duo/
