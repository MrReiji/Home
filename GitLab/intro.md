System kontroli wersji śledzi wszystkie zmiany dokonywane na pliku (lub plikach) i umożliwia przywołanie dowolnej wcześniejszej wersji.

Jeśli jesteś grafikiem lub projektantem WWW i chcesz zachować każdą wersję pliku graficznego lub
układu witryny WWW (co jest wysoce prawdopodobne), to używanie systemu kontroli wersji (VCS-
Version Control System) jest bardzo rozsądnym rozwiązaniem. Pozwala on przywrócić plik(i) do
wcześniejszej wersji, odtworzyć stan całego projektu, porównać wprowadzone zmiany, dowiedzieć
się kto jako ostatnio zmodyfikował część projektu powodującą problemy, kto i kiedy wprowadził
daną modyfikację. Oprócz tego używanie VCS oznacza, że nawet jeśli popełnisz błąd lub stracisz
część danych, naprawa i odzyskanie ich powinno być łatwe. Co więcej, wszystko to można uzyskać
całkiem niewielkim kosztem.

Source: https://github.com/progit/progit2/blob/a8da361a0fb3f5e61340be75ee23a68bc97d67a8/book/01-introduction/sections/about-version-control.asc#about-version-control
