# [[Git]]
1. [[intro]]
3. [[stany git]]
4. [[commit]]
5. [[branch]]
	
6. [[merge]]
	1. [[fast-forward merge]]
	2. [[recursive merge]]
7. [[gitignore]]

8. Konflikty [[merge conflicts]]
	1. zmodyfikowany plik ale nie w 1 miejscu
	2. zmodyfikowany plik w 1 miejscu
9. [[branching model]]
	1. Student model
	2. GitHub model
	3. GitLab model - tylko z dev, master
	4. Tagi git-a
10. [[origin]] remote [[branch]]
	1. origin/task vs task
	2. git pull a origin
11. [[protected branches]]
12. [[merge request]] / [[pull request]]
# [[GitLab]]

1. [[Gitlab for agile]]
2. [[issues]]
3. [[powiązania issue mr]] Powiązanie na przykładzie commit -> branch -> mr -> task
4. tablica [[kanban]]
5. iteracje [[sprint]]
6. Kamień milowy [[milestone]]


Źródła:
- https://git-scm.com/book/pl/v2
- Dokumentacja GitLab