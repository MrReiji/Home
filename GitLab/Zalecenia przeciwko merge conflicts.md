# Zalecenia przeciwko merge conflicts
 
 Jak zapobiec się przed taki sytuacjami? Niezbyt da się w pełni, lecz można je ograniczyć poprzez:
 - Zastosowanie odpowiedniego [[branching model]]
 - Odpowiednie planowanie. Implementacja zadań nie powinna nachodzić na takie same fragmenty kodu
 - Częste mergowanie długożyjących gałęzi. Jeśli mamy w przyszłości dołączać się do `dev`-a, możemy jego przyłączać do swojej rozwijanej gałęzi. Przez co będziemy mieli powstałe do tej pory funkcjonalności z `dev`-a,  a jak pojawią się konflikty to będą one prostsze do rozwiązania, bo mogą one być:
	 - mniejsze
	 - wykryte wcześniej, przez co ludzie będą wiedzieć o nich więcej w przypadku konsultacji, niż po miesiącu