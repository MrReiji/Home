Pliki tekstowe cechują się że git potrafi je odczytać i dokonać na nich pewnych operacji, chociażby [[recursive merge]] pliki binarne nie są takie proste do tego typu rzeczy

Często wystarczającym artefaktem pracy programisty są pliki tekstowe (też. te które zawierają lokalizacją binarnych, też. obrazy [[docker]])

Dlatego plik `.gitignore` zawiera listę nazw plików wraz ze wzorcami, które informują git-a, jakie pliki ma pomijać

## Na przykładzie frontendu

### Użycie miejsca na dysku
```sh
╰─$ du -h -d 1   
16K	    ./.vscode
13M	    ./.git
5,7M	./src
3,8M	./build        # <-----------
16K     ./public
608M	./node_modules # <-----------
631M	.
```

### Przykładowy plik gitignore

```sh
# dependencies
/node_modules

# testing
/coverage

# production
/build

# misc
.DS_Store
.env*
!.env.template
npm-debug.log*
yarn-debug.log*
yarn-error.log*
cobertura-coverage.xml
junit.xml
*coverage.cobertura.xml
.idea
```

