
Zauważmy że przy łączeniu gałęzi "master" oraz "hotfix" tak naprawdę trzeba przepiąć wskaźnik w historii commitów. Te szybkie mergowanie to "fast forward"

## przed
![[fast-forward-merge-1.png]]
# po

![[fast-forward-merge-2.png]]