Inaczej iteracje

Sprint to krótki, ograniczony czasowo okres, w trakcie którego zespół Scrum pracuje nad ukończeniem konkretnej ilości pracy.

Chodzi o właściwe zaplanowanie zadań które zespół może dowieść do końca sprint-u. W korporacjach trwa on zazwyczaj 2 tygodnie. W aplikacji internetowej, zazwyczaj jest to miesiąc.

Właściwy dobór zadań w sprincie może zapobiec sytuacją że zespół frontend-owy czeka na funkcjonalność z zespołu backend-owy. W aktualnym sprincie backend realizuje funkcjonalność, które będzie potrzebna zespołowi frontend-owemu w następnej iteracji.

Sprinty potrafią tez oszacować wydajność zespołu przez co można z lepszą predykcją oszacować terminowość funkcjonalności

![[iterations.png]]

