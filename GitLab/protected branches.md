Czy każdy powinien mieć uprawnienia przyłączania swoich zmian ? Niekoniecznie
Często dodanie commita do gałęzi długożyjących "master" / "dev" wyzwala pipeline (skrypty), które wgrywają nową wersję aplikacji na odpowiednie środowisko.

Z tego powodu są branche chronione, w GitLabie, GitHubie. O ile lokalne przyłączenie gałęzi z funkcjonalnością do chronionej może się powieść, to wypchanie tych zmian już nie.

![[protected-branches.png]]



![[piepeline.png]]
