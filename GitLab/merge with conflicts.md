# merge with conflicts

Przykład opisany w [[merge conflicts]]

A co w przypadku gdy zmodyfikowaliśmy tą samą część pliku?

Czyli idąc dalej chcemy złączyć gałąź "section1-updated" z gałęzią "master", zauważmy że gałąź "master" zostałą odłączona przed zmianami w Sekcji 1, które powstały w commicie "added section 2"

Chcąc dokonać tego scalenia, nie nadpisując zmian nie dokońca wiadomo którą wersje przyjąć i powstaje konflikt.

```bash
╰─$ git merge section1-updated
Auto-merging Readme.md
CONFLICT (content): Merge conflict in Readme.md
Automatic merge failed; fix conflicts and then commit the result.
```

![[merge-conflicts-resolve-conflicts-view1.png]]

![[merge-conflicts-resolve-conflicts-view2.png]]


W takim wypadku musimy ręcznie zadecydować która zmiana jest właściwa, przez co stworzymy commit-a z ręcznym mergem.

W tym przypadku zaakceptowałem zmiany z gałęzi "section1-updated" przez co stworzony został merge commit 

![[merge-conflicts-resolve-conflicts.png]]

```mermaid
%%{init: { 'gitGraph': { 'mainBranchName': 'master'}} }%%
gitGraph
  commit id: "Initial commit"
  commit id: "Added title"
  commit id: "Added some text"
  commit id: "Added sections placeholder"
  checkout master
  branch section2
  commit id: "added section 2"
  checkout master
  branch section1-updated
  commit id: "Added truly generated lorem ipsum to section 1"
  checkout master
  commit id: "Added section 1 text"
  merge section2 id: "Merge branch section2"
  merge section1-updated id: "Resolved conflicts while: Merge branch section1-updated"
```
 
![[Zalecenia przeciwko merge conflicts]]