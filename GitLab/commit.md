Migawka zmian w projekcie
które obecnie znajdują się w przechowalni

Czy zdarzyło się nam usunąć gotowy projekt na studia ?
Czy wprowadziliśmy zmiany w projekcie, i coś co działało, teraz przestało działać? I ctrl-Z już nie pomaga?
- Z migawką/commitem przywrócimy projekt do stanu gdzie to coś działało - znajdziemy linijkę która to psuje i z tą wiedzą poprawimy to w aktualnej wersji projektu.

Po dodaniu plików do przechowalni (stagging), tworzona jest suma kontrolna [[SHA]] z każdego z tych plików
Obiekt commit zawiera "zmiany" autora (username, email) oraz poprzednie commity w ilości:
- 0 dla pierwszego commit-a (pierwszy commit nie ma ojca)
- 1 dla najczęstszego commita (dodajemy jakiś zmiany na pliku, względem poprzedniej migawki projektu)
- 2+ dla merge commita scalającego gałęzie [[branch]] tj najnowsze commity ze scalanych branchy

### Przed modyfikacją
![[commit1.png]]

### Po modyfikacji

```bash
diff --git a/Readme.md b/Readme.md
index 4a0ced8..e546d6b 100644
--- a/Readme.md
+++ b/Readme.md
@@ -1 +1,2 @@
 # Testowe repo
+Jakiś piękny opis dodaje

```

![[commit2.png]]

![[commit-diff.png]]