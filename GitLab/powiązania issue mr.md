W GitLab-ie z poziomu issue jest możliwość utworzenia, szkicu [[merge request]]-a po kliknięciu w `Create merge request`

Opcja ta stworzy szkic MR-a, a więc z nim odrazu feture brancha, na którego zapewnie będziemy chcieli się przełączyć

![[sample-issue.png]]

## Co to daje?

Posłużę się przykładem, weźmy sobie frontend aplikacji internetowej. Dzięki git-owi możmy się dowiedzieć kiedy powstała dana linia kodu.

Poniżej w VSCode z użyciem narzędzia GitLens
![[sample-code-change.png]]


Możemy tutaj zauważyć adnotację: Changes added in [a7c8974](https://gitlab.com/spectrumagh/webappteam/frontend-WebApp/-/commit/a7c8974aadf38d372a515c046ed1640c34ccc466)
Co po otwarciu hyperlink-a przekierowuje nas na commit-a w GitLab-ie,  w nim w dolnej części możemy zauważyć w jakim [[merge request]] uczestniczył ten commit
![[sample-commit.png]]

Z niego możemy dowiedzieć się przechodząc w [[merge request]]-a [!3](https://gitlab.com/spectrumagh/webappteam/frontend-WebApp/-/merge_requests/3) z jakim issue był on powiązany

![[sample-mr.png]]

Okazuje się że jest on powiązany z issue [#2](https://gitlab.com/spectrumagh/webappteam/frontend-WebApp/-/issues/2) i przechodząc w niego możemy dowiedzieć się szczegółów tej funkcjonalności (jeśli jest)

Na tym przykładzie możemy się dowiedzieć praktycznie dlaczego każda linia kodu powstała.