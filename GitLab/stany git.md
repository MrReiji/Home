https://github.com/progit/progit2/blob/a8da361a0fb3f5e61340be75ee23a68bc97d67a8/images/areas.svg

1 -> 2 Modyfikuje pliki, aż do momentu że uznam że mogę je wysłać do poczekalni. Znajduje się tutaj migawka pliku w momencie dodania pliku do poczekalni
2 -> 3 Uznałem że to co jest w poczekalni nadaje się na zatwierdzenie, więc zatwierdzam tworząc [[commit|commita]]

3 Lokalne repozytorium

```sh
╭─szymon@Szymon-Laptop ~/repo/git_repo 
╰─$ git init
Initialized empty Git repository in /home/szymon/repo/git_repo/.git/
╭─szymon@Szymon-Laptop ~/repo/git_repo ‹master› 
╰─$ ls -a 
.  ..  .git
╭─szymon@Szymon-Laptop ~/repo/git_repo ‹master› 
╰─$ 

```