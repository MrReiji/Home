Żądanie [[merge|przyłączenia]] gałęzi z funkcjonalnością do [[protected branches]]
W GitHub pod nazwą [[pull request]]

W tym żądaniu można skonfigurować poniższe reguły:
- warunek pozytywnego przejścia pipeline, a w nim:
	- pozytywne zbudowanie obrazu
	- pozytywne przejście testów
	- brak zmniejszenie procentu pokrycia kodu testami (np. usunięcie testów)
	- brak degradacji wydajności aplikacji
	- style programowania
- Wymaganą liczbę "approve" od innych deweloperów lub "codeowners"

W skrócie kroki minimalizujące ryzyko wystąpienia błędów w aplikacji, oraz zwiększające pewność dewelopera.

![[merge-reqiest-rules.png]]