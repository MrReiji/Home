Inaczej kamienie milowe

Kamieniem milowym może być:
- prototyp aplikacji,
- api w wersji v1
- wstępnie działająca telemetria (składa się to z kilku projektów)

Są to skategoryzowane issue, które muszą zostać zrealizowane aby funkcjonalność opisana w milestone została uznana za zrealizowaną. Do niej można założyć czas rozpoczęcia i zakończenia prac.

Na tej podstawie można na wykresie oszacować np. przepalenia tj, odchylenia od szacowanej krzywej issues w funkcji czasu

![[burndown_chart_legacy_v13_6.png]]