# merge without conflicts - fast forward

Dołączmy do gałęzi master gałąź section 1, w tym przypadku użyta zostanie strategia [[fast-forward merge]]

```bash
╰─$ git merge section1
Updating ef6b470..cac669f
Fast-forward
 Readme.md | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)
```

![[merge-conflicts-fast-forward.png]]



Aktualna zawartość pliku Readme z gałęzi `master` (zawiera commita z gałęzi section 1)

```markdown
# Testowe repo
Jakiś piękny opis dodaje

# Sekcja 1

Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.

# Sekcja 2

todo
```