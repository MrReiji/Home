# merge without conflicts - recursive

Teraz gałąź sekcja 2 nie zawiera commita sekcji 1, która już znajduje się w gałęzi master, więc nie ma wspólnego potomka więc trzeba zastosować złączenie [[recursive merge]] z merge commitem
Aktualna sytuacja (bez gałęzi `section1` dla uproszczenia)

```mermaid
%%{init: { 'gitGraph': { 'mainBranchName': 'master'}} }%%
gitGraph
  commit id: "Initial commit"
  commit id: "Added title"
  commit id: "Added some text"
  commit id: "Added sections placeholder"
  checkout master
  branch section2
  commit id: "added section 2"
  checkout master
  branch section1-updated
  commit id: "Added truly generated lorem ipsum to section 1"
  checkout master
  commit id: "Added section 1 text"
```

Zawartość pliku Readme z gałęzi "master"

```markdown
# Testowe repo
Jakiś piękny opis dodaje

# Sekcja 1

Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.

# Sekcja 2

todo
```

Zawartość pliku `Readme` z gałęzi "section2", która zmodyfikowała w tym samym pliku sekcje 2

```markdown
# Testowe repo
Jakiś piękny opis dodaje

# Sekcja 1

todo

# Sekcja 2

It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
```

Jak można zauważyć, gałąź "section1" nie zawiera najnowszych zmian z master-a (tj uzupełnionej sekcji 1).
Więc merge commit musi uwzględnić aktualne zmiany w gałęzi master oraz nowe zmiany z gałęzi section2

W wyniku poniższego polecenia, po zaakceptowaniu domyślnej wiadomości merge commita
```
╰─$ git merge section2
Auto-merging Readme.md
Merge made by the 'ort' strategy.
 Readme.md | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)
# ort == recursive
```

Do branchy master zostaną dodane zmiany z gałęzi section2, z commit-a,  z racji tego że tam jest zawarta informacja o różnicy, to dodana różnica / migawka / [[commit]] w efekcie **nie** usunie/ **nie** nadpisze sekcji 1, a doda brakującą sekcje 2

A po naszej operacji w gałęzi "master" zawarta będzie praca z gałęzi "section1" oraz "section2"
![[merge-conflicts-recursive.png]]


```mermaid
%%{init: { 'gitGraph': { 'mainBranchName': 'master'}} }%%
gitGraph
  commit id: "Initial commit"
  commit id: "Added title"
  commit id: "Added some text"
  commit id: "Added sections placeholder"
  checkout master
  branch section2
  commit id: "added section 2"
  checkout master
  branch section1-updated
  commit id: "Added truly generated lorem ipsum to section 1"
  checkout master
  commit id: "Added section 1 text"
  merge section2 id: "Merge branch 'section2"
```