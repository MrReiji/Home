GitLab tablicę kanban, tj miejsce gdzie możemy graficznie zobaczyć stan projektu lub projektów.
Issue board może być zarówno dla projektu (wtedy widzimy issues tylko z wybranego projektu), jak i dla grupy (wtedy widzimy issues z projektów w grupie)

Poniżej issue board dla grupy web app

![[kanban-dev.png]]

GitLab oferuje możliwość dodania kilku kolumn ww. tablicy, można to zrobić dodając tzw. Labels. A kolumna będzie zawierała taski z wybranej labelki. Poniżej przykład innej tablicy z labelkami.

Początek tablicy
![[kanban-full-1.png]]

Koniec tablicy
![[kanban-full-2.png]]

W aplikacji internetowej funkcjonuje poniższy stan kolumn i labelek:
- Open (bez labelki): issue do podjęcia przez deweloper-a
- Priority High: Wysoki priorytet
- Dev In progress: Funkcjonalność w trakcie rozwijania
- Dev Done: Zakończono rozwijanie funkcjonalności, oczekiwanie na review
- Review In progress: Inny dewelopera sprawdza implementację pod kątem jakości kodu oraz testuje spełnienie założeń w issue (na dzień pisania tych informacji nie ma zespołu testującego)
- Review Done: Zakończono sprawdzenia implementacji
	- Jeśli jest ona jakościowo akceptowalne dajemy approve [[merge request]] i deweloper wykonuje złączenie [[branch]]-y
	- Jeśli nie, piszemy uwagi w komentarzach (w GitLab-ie) i deweloper rekursywnie wraca do punku Dev In progress

Istnieje możliwość dodania tablicy dla konkretnego [[sprint|sprintu/iteracji]]

