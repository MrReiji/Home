https://www.waveshare.com/wiki/5inch_HDMI_LCD_(B)_(Firmware_Rev_2.1)_User_Manual

Flutter na RPI: https://medium.com/snapp-embedded/flutter-on-raspberry-pi-raspi-flutter-e1760818ba0c (robiłem z root-a podając ścieżkę bezwzględną snap-a)

## PCB

![[20231221_191907.jpg]]

## Podłączenie do PC

Dotyk po przewodzie microUSB  typ A <-> USB widoczny jako `WaveShare WS170120`

```bash
[szymon@localhost documentation]$ sudo usb-devices
T:  Bus=03 Lev=01 Prnt=01 Port=00 Cnt=01 Dev#=  7 Spd=12   MxCh= 0
D:  Ver= 2.00 Cls=00(>ifc ) Sub=00 Prot=00 MxPS=64 #Cfgs=  1
P:  Vendor=0eef ProdID=0005 Rev=02.00
S:  Manufacturer=WaveShare
S:  Product=WS170120
S:  SerialNumber=<ÎH4410W161
# ...
```

Podłączony do laptopa przez HDMI (RHEL 9.3), obsługuje FullHD 60 fps.


![[Pasted image 20231221200934.png]]
