Używa [[docker]]-a

```bash
docker run -it -p 1883:1883 -v ./mosquitto.conf:/mosquitto/config/mosquitto.conf eclipse-mosquitto 
```

Gdzie w pliku `mosquitto.conf` [[mosquitto#Otwarcie portów i anonimowy użytkownik]] musi być podstawowa konfiguracja