Z tego co się dowiedziałem to: 
* tarcze absu mają dużą rozdzielczość 
* pomiar prędkości przy takiej rozdzielczości działa trochę inaczej niż na wolnych prędkościach 
* Trzeba znaleźć naprawdę szybkie wejście i liczyć ile impulsów jest w danym czasie zamiast czas między impulsami

9.02.2024
1. [Jak ma działać ABS](https://www.youtube.com/watch?v=wlVn4pW_NAc)
2. [Artykuł_1_arduino](https://forum.arduino.cc/t/abs-sensor-scooter-arduino/422067?fbclid=IwAR2L4GRgh_4J_wtpf0srifponqCthY3USl8B3uBEfPf-PGTsRR-Na3GdeT4)
	Ziomeczek użył wzmacniacza operacyjnego do przekształcenia wyjścia czujnika w stan logiczny dla arduino.
3. [Filmik_arduino](https://www.youtube.com/watch?v=AR52UUv2zLs)
		[Kod](https://www.rigidracing.com/2018/10/06/reading-abs-wheel-speed-sensors-with-arduino/)

Trzeba by przekształcić to na stm32

# 13.02

Wniosek na 5V ciężko żeby abs wykazywał zmiany, wtedy trzeba mniejszy rezystor do masy

Ustawienia na 12V

## Vcc 12v

#### Rezystancja między sygnał abs a masą: 460 ohm

Sygnał ABS
- stan wyższy: 7.4 V
- stan niższy: 4.0V

Referencja komparatora (środek ww. wartości abs-u) ok 5.2V

Wyjście pull-up przez rezystor 10 kOhm tj logiczna 1 - 12V
dokładnie:
- zero - 0,4V
- jedynka - 11,2 V

Ale że to otwarty dren to dałem pull up 2,7 V pod stm-ke

![[Pasted image 20240214224826.png]]