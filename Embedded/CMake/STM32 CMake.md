Aby móc tworzyć projekty niezależne od providera IDE, jednym z rozwiązań jest CMake.

W przypadku rozwiązań STM32 interesującym będzie STM CubeMX, a szczególnie opcja tworzenia projektu do pliku Makefile. Zawiera on kompletne stałe, definicje, przełączniki do ustalenia i przekazania kompilatorowi, przez makefile oraz w tym przypadku przez cmake.

Cmake jest wykorzystywany przez edytory takie jak VSCode łącznie z InteliSense, STM32 IDE od wersji 1.14 również wspiera Cmake.

Istotnym jest ustawienie `STM32_FW_Emoto` na potrzeby firmware cmake. [[STM32_FW_Emoto]]

Podczas tworzenia projektu można wyróżnić kolejne kroki:
[[Generowanie w CubeMX]]
[[Plik CMake]]
[[Konfiguracja CMakeLists.txt]]
[[Konfiguracje CMakePresets.json]]
[[VSCode Cmake InteliSense]]