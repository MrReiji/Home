Zalecany workflow w VSCode składa się na pluginy znajdujące się w pliku `extensions.json`
Podczas pierwszego uruchomienia projektu VSCode zapyta o to czy zinstalować rekomandowane rozszerzenia które zdefioniowano w poniższym pliku

```json
{
    "recommendations": [
        "ms-vscode.cpptools",
        "ms-vscode.cmake-tools",
        "marus25.cortex-debug",
    ]
}
```

Lub przechodząc w zakładki z pokazanym filtrem `@recommended`

![[Pasted image 20240219121430.png]]

Oraz wsparcie dla cmake w pliku `c_cpp_properties.json`

```json
{
    "configurations": [
        {
            "name": "STM",
            "configurationProvider": "ms-vscode.cmake-tools",
            "intelliSenseMode": "${default}",
            "includePath": [
                "${default}",
                "${env:STM32_FW_Emoto}/Drivers/STM32L4xx_HAL_Driver/Inc",
                "${env:STM32_FW_Emoto}/Drivers/STM32L4xx_HAL_Driver/Inc/Legacy",
                "${env:STM32_FW_Emoto}/Drivers/CMSIS/Device/ST/STM32L4xx/Include",
                "${env:STM32_FW_Emoto}/Drivers/CMSIS/Include"
            ]
        }
    ],
    "version": 4
}
```