Plik rozszerzeniu `.cmake` tzw. `CMAKE_TOOLCHAIN_FILE` można powiedzieć że zawiera spis jaki użyć kompilator, dla STM32 głownie opiera się to o `arm-none-eabi-*` co jest zaznaczone w zmiennej `TOOLCHAIN_PREFIX`

Bez niego "coś" się skompiluje jeśli domyślny kompilator rozpozna flagi kompilatora specyficzne dla domyślnego gcc

Plik ten jest przekazywany do generowania konfiguracji przez cmake lub (częściej) w [[Konfiguracje CMakePresets.json]]

```cmake
set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_PROCESSOR arm)
  
# Some default GCC settings
# arm-none-eabi- must be part of path environment
set(TOOLCHAIN_PREFIX arm-none-eabi-)
  
# Define compiler settings
set(CMAKE_C_COMPILER ${TOOLCHAIN_PREFIX}gcc)
set(CMAKE_ASM_COMPILER ${CMAKE_C_COMPILER})
set(CMAKE_CXX_COMPILER ${TOOLCHAIN_PREFIX}g++)
set(CMAKE_OBJCOPY ${TOOLCHAIN_PREFIX}objcopy)
set(CMAKE_SIZE ${TOOLCHAIN_PREFIX}size)
  
set(CMAKE_EXECUTABLE_SUFFIX_ASM ".elf")
set(CMAKE_EXECUTABLE_SUFFIX_C ".elf")
set(CMAKE_EXECUTABLE_SUFFIX_CXX ".elf")
  
set(CMAKE_TRY_COMPILE_TARGET_TYPE STATIC_LIBRARY)
```