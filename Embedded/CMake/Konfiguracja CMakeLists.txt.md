Pliki tego formatu głównie służą do opisu jak skompilować, złączyć itp projekt. Po więcej informacji odsyłam do innych źródeł. Tutaj będzie przedstawiony specyficzny scenariusz dla STM32

## Cmake include

Jedną z zalet jest możliwość `inlcude` wspomnianych plikach które znajdują się w podfolderach. Przez co w podfolderze jest konfiguracja plików (głównie spis plików) która zawiera kilka linii.

```cmake
include("${PROJ_PATH}/Core/CMakeLists.txt")
```
Sama kompletna zawartość załączonego pliku została pokazana poniżej
```cmake
cmake_minimum_required(VERSION 3.22) 
AUX_SOURCE_DIRECTORY("${CMAKE_CURRENT_LIST_DIR}/Src" CoreSrc)
set(CoreInc ${CMAKE_CURRENT_LIST_DIR}/Inc)
```

W wyniku jego działania zostanie skonstruowana lista wszystkich plików źródłowych w katalogu (Cmake ogarnia jakie to rozszerzenia plików) w definicji `CoreSrc` oraz include w `CoreInc`

Definicje te są przekazywane do głównego pliki `CMakeLists.txt` w poniższy sposób

```cmake
set(Src # Src przekazany jest do kompilacji
	${StartupFile}
	${CoreSrc} # Opisywany przykład
	${FwSrc}
)
```

## Cmake STM32 Firmware

Teoretycznie można wykorzystać podejście z korku powyższego, ale spowoduje to poniższy błąd

```bash
[  1%] Linking C executable front_wheel.elf
/usr/lib/gcc/arm-none-eabi/10.3.1/../../../arm-none-eabi/bin/ld: CMakeFiles/front_wheel.dir/home/szymon/STM32Cube/Repository/STM32Cube_FW_L4_V1.18.0/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_msp_template.c.obj: in function `HAL_MspInit':
/home/szymon/STM32Cube/Repository/STM32Cube_FW_L4_V1.18.0/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_msp_template.c:54: multiple definition of `HAL_MspInit'; CMakeFiles/front_wheel.dir/Core/Src/stm32l4xx_hal_msp.c.obj:/home/szymon/repo/embedded/front_wheel/Core/Src/stm32l4xx_hal_msp.c:65: first defined here
```

Redefinicja, powodem jest to że chcemy dodać całe firmware, a nie część. Co ciekawe w firmware plik `/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_msp_template` zawiera już definicję do `HAL_MspInit`

W naszym przypadku chcemy dołączyć tylko część firmware dlatego patrząc na fragment pliku `make`
```make
/home/szymon/STM32Cube/Repository/STM32Cube_FW_L4_V1.18.0/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_tim.c \
/home/szymon/STM32Cube/Repository/STM32Cube_FW_L4_V1.18.0/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_tim_ex.c \
/home/szymon/STM32Cube/Repository/STM32Cube_FW_L4_V1.18.0/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_uart.c \
/home/szymon/STM32Cube/Repository/STM32Cube_FW_L4_V1.18.0/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_uart_ex.c \
/home/szymon/STM32Cube/Repository/STM32Cube_FW_L4_V1.18.0/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal.c \
/home/szymon/STM32Cube/Repository/STM32Cube_FW_L4_V1.18.0/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_rcc.c \
/home/szymon/STM32Cube/Repository/STM32Cube_FW_L4_V1.18.0/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_rcc_ex.c \
/home/szymon/STM32Cube/Repository/STM32Cube_FW_L4_V1.18.0/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_flash.c \
/home/szymon/STM32Cube/Repository/STM32Cube_FW_L4_V1.18.0/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_flash_ex.c \
/home/szymon/STM32Cube/Repository/STM32Cube_FW_L4_V1.18.0/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_flash_ramfunc.c \
/home/szymon/STM32Cube/Repository/STM32Cube_FW_L4_V1.18.0/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_gpio.c \
/home/szymon/STM32Cube/Repository/STM32Cube_FW_L4_V1.18.0/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_i2c.c \
/home/szymon/STM32Cube/Repository/STM32Cube_FW_L4_V1.18.0/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_i2c_ex.c \
/home/szymon/STM32Cube/Repository/STM32Cube_FW_L4_V1.18.0/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_dma.c \
/home/szymon/STM32Cube/Repository/STM32Cube_FW_L4_V1.18.0/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_dma_ex.c \
/home/szymon/STM32Cube/Repository/STM32Cube_FW_L4_V1.18.0/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_pwr.c \
/home/szymon/STM32Cube/Repository/STM32Cube_FW_L4_V1.18.0/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_pwr_ex.c \
/home/szymon/STM32Cube/Repository/STM32Cube_FW_L4_V1.18.0/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_cortex.c \
/home/szymon/STM32Cube/Repository/STM32Cube_FW_L4_V1.18.0/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_exti.c \
```

Na tej podstawie możemy wydedukować jakie pliki mogą być dodane

```cmake
set(FwSrc 
    "$ENV{STM32_FW_Emoto}/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_tim.c"
    "$ENV{STM32_FW_Emoto}/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_tim_ex.c"
    "$ENV{STM32_FW_Emoto}/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_uart.c"
    "$ENV{STM32_FW_Emoto}/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_uart_ex.c"
    "$ENV{STM32_FW_Emoto}/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal.c"
    "$ENV{STM32_FW_Emoto}/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_rcc.c"
    "$ENV{STM32_FW_Emoto}/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_rcc_ex.c"
    "$ENV{STM32_FW_Emoto}/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_flash.c"
    "$ENV{STM32_FW_Emoto}/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_flash_ex.c"
    "$ENV{STM32_FW_Emoto}/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_flash_ramfunc.c"
    "$ENV{STM32_FW_Emoto}/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_gpio.c"
    "$ENV{STM32_FW_Emoto}/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_i2c.c"
    "$ENV{STM32_FW_Emoto}/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_i2c_ex.c"
    "$ENV{STM32_FW_Emoto}/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_dma.c"
    "$ENV{STM32_FW_Emoto}/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_dma_ex.c"
    "$ENV{STM32_FW_Emoto}/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_pwr.c"
    "$ENV{STM32_FW_Emoto}/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_pwr_ex.c"
    "$ENV{STM32_FW_Emoto}/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_cortex.c"
    "$ENV{STM32_FW_Emoto}/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_exti.c"
)
```

Z jednej strony jest to wada jak i zaleta.
- Wada, bo trzeba ręcznie to dodawać.
- Zaleta (z doświadczenia) musiałem kiedyś wykluczyć definicję tabeli USB HID bo STM32 nie definiował takiej jaką ja chciałem, więc musiałem zrobić ją sam i zastąpić problematyczny plik. Jak? poprostu usunąć w powyższym pliku i dodać swój.

[Dla hejterów organiczenie cmake w stm32 cube](https://community.st.com/t5/stm32-mcus-products/stm32cubemx-feature-request-possibility-to-specify-relative/td-p/339945)
## Główna konfiguracja CMake

Natomiast główna konfiguracja jest dosyć skomplikowana, ale często tworzona tylko raz na podstawie wygenerowanego pliku `make`w kroku [[Generowanie w CubeMX]]

W dużej mierze stałe mogą być przepisane z `make` do `cmakelist` 1 do 1, co też jest pożądane żeby nie było różnic gdy kompiluje się make oraz cmake

### Poniżej fragment pliku `make`

```make
#######################################
# CFLAGS
#######################################
# cpu
CPU = -mcpu=cortex-m4
  
# fpu
FPU = -mfpu=fpv4-sp-d16
  
# float-abi
FLOAT-ABI = -mfloat-abi=hard
  
# mcu
MCU = $(CPU) -mthumb $(FPU) $(FLOAT-ABI)
  
# macros for gcc
# AS defines
AS_DEFS =
  
# C defines
C_DEFS = \
-DUSE_HAL_DRIVER \
-DSTM32L432xx
```

### Oraz poniżej fragment pliku `CMakeLists`

```cmake
# ######################################
# CFLAGS
# ######################################
# cpu
set(CPU -mcpu=cortex-m4)
  
# fpu
set(FPU -mfpu=fpv4-sp-d16)
  
# float-abi
set(FLOAT-ABI -mfloat-abi=hard)
  
# mcu
set(MCU
	${CPU}
	-mthumb
	${FPU}
	${FLOAT-ABI}
)
  
# macros for gcc
# AS defines
set(AS_DEFS)
  
# C defines
set(C_DEFS
	"USE_HAL_DRIVER"
	"STM32L432xx"
)
```

W gruncie rzeczy to są same definicje oraz specyficzne cmakeList pliki