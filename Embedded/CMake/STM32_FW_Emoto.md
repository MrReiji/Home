## Lokalizacja Firmware

Do poprawnego zainstalowania potrzebne jest ustawienie zmiennej środowiskowej `STM32_FW_Emoto`. Jej wartość wskazuje lokalizację firmware wraz z poprawną wersją, co może być znalezione w ustawieniach CubeMX których fragment pokazano poniżej.

![[Mcu and firmware package.png]]
  
```bash

export STM32_FW_Emoto=/home/szymon/STM32Cube/Repository/STM32Cube_FW_L4_V1.18.0

```
  

Dla linux-a aby zmienna była stała można dodać wpis do `/etc/profile.d`. Dla windows-a zakładka zmienne środowiskowe

```bash
echo 'export STM32_FW_Emoto=/home/szymon/STM32Cube/Repository/STM32Cube_FW_L4_V1.18.0' | sudo tee /etc/profile.d/stm32-fw-emoto.sh
```

  