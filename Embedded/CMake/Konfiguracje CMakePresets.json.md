Jednym z problemów, z którymi często borykają się użytkownicy CMake, jest udostępnianie ustawień innym osobom dotyczących wspólnych sposobów konfiguracji projektu, co jest rozwiązywane w tym pliku i powinno być respektowane przez IDE (np. VSCode)

Poniżej jest wskazany plik z toolchainem [[Plik CMake]] oraz dwie konfiguracje, aby konfiguracje cmake były parsowane do `makefile` oraz `ninja` (szybsza)

```json
{
    "version": 3,
    "configurePresets": [
        {
            "name": "Ninja",
            "generator": "Ninja",
            "binaryDir": "${sourceDir}/build/",
            "toolchainFile": "${sourceDir}/../STM32.cmake",
            "cacheVariables": {
                "CMAKE_EXPORT_COMPILE_COMMANDS": "ON",
                "CMAKE_MAKE_PROGRAM": "ninja"
            }          
        },
        {
            "name": "Makefiles",
            "binaryDir": "${sourceDir}/build/",
            "toolchainFile": "${sourceDir}/../STM32.cmake",
            "cacheVariables": {
                "CMAKE_EXPORT_COMPILE_COMMANDS": "ON",
                "CMAKE_MAKE_PROGRAM": "make"
            }          
        }
    ]
}
```