Aby uzyskać "Framework agnostic" opis kompilacji projektu powinien zostać przeprowadzony do np. makefile. W CubeMX służy do tego opcja `Toolchain / IDE` makefile.

Plik ten będzie wykorzystany przy [[Konfiguracje CMakePresets.json]]

Aby uniknąć kopiowania plików "firmware STM32" do repozytorium co spowoduje jego spuchnięcie lepszym pomysłem jest dodanie referencji "include". Poniższa opcja pokazana poniżej

![[Pasted image 20240219104359.png]]

Spowoduje to wykorzystanie firmware z lokalizacji wskazanej poniżej `Firmware Relative Path` (default location)

![[Pasted image 20240219104448.png]]

Przy pierwszym uruchomieniu wspomniane firmware może nie być pobrane, dlatego CubeMx przy pierwszym kliknięciu `Generate code` upomni się o pobranie, co jest dla nas pomocne.

![[Pasted image 20240228220318.png]]

Ostatnią wartą uwagi opcją jest generowanie kodu w osobnych plikach `.h /c`