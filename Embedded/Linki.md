## **Projekty Embedded**
1. [Hackster](https://www.hackster.io/) Znajdziesz na niej wiele projektów, można inspirować się tymi rozwiązaniami

## Symulator embedded
1. [Wokwi](https://wokwi.com/) Na tej stronie można zasymulować działanie paru elementów, strona wiele rzeczy nie ma, wiele jej brakuje, jednakże da się.
## Strony do apek
1. [Thingspeak](https://thingspeak.com/)
2. [Blynk](https://blynk.io/)
3. [Appinventor](https://appinventor.mit.edu/)
## PCB
1. [EasyEda](https://easyeda.com/pl) w 100% darmowa
2. [Eagle](https://www.autodesk.com/products/eagle/overview?term=1-YEAR&tab=subscription) konto studenckie
3. [Altium](https://www.altium.com/)
## Przydatne repo
1. [Piotr Duba - Forbot](https://github.com/ptrre?tab=repositories) 
2. [Mateusz Salamon](https://github.com/lamik?tab=repositories)

## Organizacja
1. [Lettucemeet](https://lettucemeet.com/)
2. [Lucidchart](https://www.lucidchart.com/pages/) 




