 - [ ] Rozrysować połączenia magistrali CAN 
	 - Sterownik, BMS, IMD, STM32, RPI
	 Uważam, że jest to potrzebne wtedy każdy widzi w jaki sposób jest to połączone ze sobą
	 Myślę, że w jakimkolwiek programie graficznym się to zrobi
- [ ] Update strony [E-MOTO](http://www.e-moto.agh.edu.pl/)
- [ ]  Zapis telemetrii na kartę SD
- [ ] Uskok amortyzatora, przód i tył 
- [ ] OCI 

- [ ]  Zapis telemetrii na kartę SD
	- [ ] Zapisanie danych w odpowiedniej kolejności

- [ ] Akcelerometr [MPU6050](https://botland.com.pl/zyroskopy/3888-mpu-6050-3-osiowy-akcelerometr-i-zyroskop-i2c-modul-dfrobot-6959420908240.html)
	- [x] Pochylenie motocykla w poziomie -90 do +90 stopni
	- [ ] Odchył w pionie
	- [ ] Przyśpieszenie

- [ ] Czujnik ABS
	- [x] Montażem tego w moto zajmują się już mechanicy
	- [x] Zmontowanie układu z komparatorem
	- [x] Odczyt impulsów
	- [ ] Pomiar prędkości 
	- [ ] Mierzenie dystansu
	- [ ] Średnia prędkość na ostatnie 10km
	
- [ ] CAN
	- [x] Przesyłanie ramek CAN do raspberry
	- [x] Zapis danych z akcelerometru MPU6050 do ramki CAN
	- [ ] Zapis danych pomiaru temperatury baterii  do ramki CAN
	- [ ] Zapis danych pomiaru temperatury cieczy chłodzącej do ramki CAN
	- [ ] Zapis danych z czujnika ABS do ramki CAN
	- [ ] Zapis danych z czujników TPMS do ramki CAN
	- [ ] Zapis danych pomiaru napięcia na akumulatorze do ramki CAN
	- [ ] Zapis danych pomiaru prądu naładowania akumulatora do ramki CAN
	- [ ] Zapis danych czasu do pełnego naładowania do ramki CAN
	- [ ] Zapis danych zasięgu akumulatora do ramki CAN
	
- [ ] Akumulator
	- [ ] Pomiar napięcia akumulatora
	- [ ] Pomiar prądu naładowania akumulatora
	- [ ]  Czas do pełnego naładowania
	- [ ] Zasięg akumulatora

- [ ] Temperatury
	- [ ] Pomiar temperatury wody (czujka DS18B20)
	- [ ] Pomiar temperatury baterii
		* W baterii mamy 4 [Termistory](https://botland.com.pl/blog/termistor-definicja-zastosowania-i-dzialanie/), z których będziemy odczytywać i uśredniać temperature.

- [ ] TPMS (Tire Pressure Monitoring System)
	- [ ] Ogarnięcie jaki TPMS będzie odpowiedni, łatwy w obsłudze, skuteczny, przesyłanie danych po CAN'ie
	- [ ] Ogarnięcie jak odczytywać dane z niego
	- [ ] Odczyt danych z TPMS

- [ ] Test Bench
	* Należy zrobić to tak aby było łatwe do demontażu, np poprzez zastosowanie jakiś holderów wydrukowanych w 3D
	- [ ] Ogarnięcie jakiejś drewnianej płyty, na której będzie cały mechanizm
	- [ ] Zamontowanie wyświetlacza
	- [ ] Zamontowanie mikrokontrolerów (raspberry, stm32)
	- [ ] Zamontowanie czujnika temperatury DS18B20
	- [ ] Zamontowanie termistorów
	- [ ] Zamontowanie jakiejś platformy, typu ten kołowrotek (możemy także nową wydrukować)
	- [ ] Zamontowanie układu z komparatorem dla czujnika ABS
	- [ ] 


