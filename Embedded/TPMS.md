[Nasz TPMS](https://allegro.pl/oferta/czujnik-cisnienia-opon-tpms-android-bluetooth-4szt-14142665556)
[Nasz Aktualny TPMS, Schrader Electronics](https://catalogue.schradertpms.com/en-US/ProductDetails/20213.html)
## Przykładowe projekty
1) [TPMS](https://www.hackster.io/jsmsolns/arduino-tpms-tyre-pressure-display-b6e544)
2) [Biblioteka CC1101 dla L432KCU](https://github.com/suleymaneskil/CC1101_STM32_Library)
# Elementy
* [Antena](https://nettigo.pl/products/modul-cc1101-transceiver-433-mhz-z-antena)

# WAŻNE
1) [ RTL433 Arduino Lib & RTL433  TO MQTT](https://hackaday.com/2023/01/13/arduino-library-brings-rtl_433-to-the-esp32/)
2) [Odkodowanie, reverse engineering](https://github.com/merbanan/rtl_433/wiki/Decipher-a-new-device-and-write-a-decoder#3-a-strategy-for-reverse-engineering-a-device)
3. [RTL 433 ](https://github.com/merbanan/rtl_433)
4. Zadzwoniłem do felgeo, mają jeden zamiennik tpms marki CUB i programatorem programują pod konkretny protokół, więc mogą nam zapropogramować dosłownie cokolwiek, w razie problemów ze zwrotem nie będzie problemów [LINK ](https://felgeo.pl/pol_m_Systemy-TPMS_Zamienne-czujniki-cisnienia-TPMS-345661.html?counter=3)

# CO SIE UDAŁO ZROBIĆ
 *  Udało sie odczytać sygnał z jednego z Tpmsów, mamy podane ładnie ciśnienie (Słoik z nim nie jest niczym oznaczony)
 *  Drugi TPMS pokazuje tylko bity, oznaczyłem słoik z nim "X", na samym TPMSIE też jest X
 * Jak to zrobiliśmy?
 -Pobranie sterowników do dongla RTL SDR z linku quickstart guide
 -Pobranie RTL43
 -Zainstalowanie pluginu do SDRSharp z poniższego linku ([tutorial](https://www.youtube.com/watch?v=EhKfvSJ0Ktk))
 
 Poprawnie odbierany TPMS z ładnym info:![[Pasted image 20240510164537.png]]
# Co dalej?
- Trzeba kupić obsługiwany TPMS, listę obsługiwanych możemy znaleźć pod tym linkiem ([supported device protocols](https://github.com/merbanan/rtl_433)): 
- Skminić jak przesłać to na mikokontroler (patrzeć przykładowy projekt od Grześka)
- Uwaga, [RTL433 ARDUINO](https://github.com/NorthernMan54/rtl_433_ESP) Działa i na ESP [Link ](https://www.youtube.com/watch?v=H-JXWbWjJYE)
- 
# R&D
*  Znalezione TPMSy (Ciekawsze propozycje):
    - [TPMS Felgeo Toyota Auris Zamiennik z przykładowego projektu - instrukcja pod programator ](https://felgeo.pl/product-pol-22407-Czujnik-cisnienia-opon-TPMS-do-TOYOTA-AURIS-01-2014-12-2018-433MHZ.html) [Instrukcja (Szczególna Uwaga podpunkt 4)](https://felgeo.pl/data/links/24ae59f18d2721992c1b391a9a39da8c/72996_2397.pdf)
    - [TPMS Premium Felgi ](https://www.premiumfelgi.pl/pl/p/Czujnik-cisnienia-TPMS-433-MHz/5572)
    - [TPMS Oponeo ](https://www.oponeo.pl/czujnik-cisnienia-tpms?gad_source=1)
    - [Strona TaniTPMS, czyli dziwny TPMS z panelem solarnym ](https://tanitpms.pl/pl/p/Solarny-system-TPMS-z-4-czujnikami-wewnetrznymi./40)
*  Do wszystkich sklepów wysyłałem maile, obdzwonię każdy z nich w poniedziałek bo większość nie pracuje w weekend majowy [Nie wiedzą o co chodz]
*  Zadzwoniłem do oponeo, nie zrozumieliśmy się, gość poprosił o mail [Nie odpisali :c]

Pomysły: 
- Wzbudzenie przy wykorzystaniu słoika i pompowaniu do niego powietrza [✔]
- Możnaby spróbować wykorzystać zamiennik z przykładowego projektu (ten wyżej), kupić czujnik z Toyoty Auris i zrobić coś podobnego co on [Tak trzeba chyba]

Problemy:
- ~~Cięzko znaleźć dobrą dokumentację techniczną, firmy niechętnie udostępniają dane bo nie jest to w ich interesie~~ [Nie jest tam potrzebna]
- ~~Wzbudzanie czujnika do działania jest problematyczne~~ [Nie jest]



