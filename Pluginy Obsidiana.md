
Obsidian wykorzystuje folder `./.obsidian` w repozytorium. W nim zapisana jest konfiguracja programu jak i do tego folderu zapisywane są pluginy.

Aby mogły być one użyte należy je włączyć przechodząc `Settings (icon)` -> `Community plugins` -> `Turn on community plugins`
Pluginy można zainstalować wpisując nazwę w `Settings (icon)` -> `Community plugins` -> `Browse`

Aby plugin działał należy kliknąć `Install` oraz `Enable`

O ile ustawienia systemowych pluginów się [synchornizują](./.gitignore) to reszta konfiguracji jest up to you. Niemniej jednak zalecam kilka pluginów community:

1. [Obsidian Git](https://github.com/denolehov/obsidian-git) wsparcie Git-a w UI
   - Zalecam uruchomienie okien wpisując w oknie komend `Ctrl+P` obsidiana poniższe komendy
      1. Open Source Control View
      2. Open History View
2. [Templater](https://github.com/SilentVoid13/Templater) Rozszerzenie funkcjonalności szablonów
3. [Excalidraw](https://github.com/zsviczian/obsidian-excalidraw-plugin) [Projekt](https://excalidraw.com/) bardziej rozbudowanej wersji Canvy, ale nie wspierany przez Markdowna
4. [Dataview](https://github.com/blacksmithgu/obsidian-dataview) Ala SQL w Obsidianie
5. [Omnisearch](https://github.com/scambier/obsidian-omnisearch) Dużo lepsza wyszukiwarka
   - Do wyszukiwania na obrazach (OCR) jest rozszerzenie [Obsidian Text Extractor](https://github.com/scambier/obsidian-text-extractor)

