Założenie aplikacji jest takie że jak raz przypiszemy identyfikator danemu pomiarowi, to nie będzie się on zmieniał. W przeciwnym razie, powiedzmy że prędkość ma id 100, i zaczniemy w [[mqtt]] poprzez [[konfiguracja can2mqtt]] w ten identyfikator zapisywać ciśnienie to:
- Na pozycji prędkości na wyświetlaczu pojawi się ciśnienie
- W aplikacji internetowej będziemy mieć zmieszane pomiary prędkości i ciśnienia

Jeśli w jednym motocyklu będziemy mieć ciśnienie o id 100, a w drugim 200, nie będą dostępne możliwości wyciągnięcia średniej z każdego motocykla.

A więc, **identyfikatory przetworzonych pomiarów nie mogą się dublować ani zmieniać**
Ramki CAN mogą być różne ale po przetworzeniu muszą mieć takie same id

Id może być tylko i wyłącznie typem `uint16_t` (względy optymalizacyjne) a więc przyjmuje wartości od `0 - 65535` 

Aplikacje rozpatrują dane typu:
- double: po prostu numer (99 % przypadków - dlatego domyślnie się o tym tylko mówi)
- location: pozycja geograficzna. struktura:
	- Lon: double
	- Lat: double

Rozpatrywanie lokalizacji jako dwie wartości double nie kończyło się dobrze (periodyczne zygzakowanie)

---

### Pogrupowane id danych telemetrycznych - double (zmiennoprzecinkowych)

| Zakres ID (dec) | Funkcjonalność                                                                                                           | Komentarz                                                                                               |
| --------------- | ------------------------------------------------------------------------------------------------------------------------ | ------------------------------------------------------------------------------------------------------- |
| 50-99           | Dane z samego RPI                                                                                                        | Jakieś metryki diagnostyczne z samej rpi np. wolna pamięć temperatura procesora                         |
| 100-109         | Prędkości                                                                                                                | np. przedniego koła                                                                                     |
| 110-119         | Zmiany zawieszenia                                                                                                       | np. ugięcie amortyzatora                                                                                |
| 120-129         | TPMS-y                                                                                                                   | np. ciśnienie opon                                                                                      |
| 130-199         | Reserved                                                                                                                 | Zarezerwowane na przyszłe rzeczy głownie od [[stm32]]                                                   |
| 200-299         | Temperatury (bez baterii)                                                                                                | np. cieczy, dolotu, termistory                                                                          |
| 300-999         | Reserved                                                                                                                 | Zarezerwowane na przyszłe rzeczy głownie od [[stm32]]                                                   |
| 1000-1999       | Napięcia ogniw                                                                                                           | Pierwsze ogniwo to 1000, drugie 1001, 178 to 1178                                                       |
| 2000-2999       | Pojemności ogniw                                                                                                         | jw, ale dla pojemoność dla odpowiednich ogniw, NIE SPRAWDZONE jeszcze                                   |
| 3000-9999       | Reserved                                                                                                                 | Zarezerwowane na przyszłe rzeczy głownie od [[bms]]-a                                                   |
| 10000-19999     | Sevcon (ogólniej sterownik silnika). Poniżej podano informacje odzwierciedlające kolumnę `Index` z dokumentacji sevcon-a | Dane pochodzące ze sterownika silnika, Excel [[sevcon]]-a ma ich kilka tysięcy (ale to by zabiło can-a) |
| 10000-10069     | Sevcon 0x4602: AC Motor Debug Information                                                                                | Subindex 0 - 66                                                                                         |
| 10070-10099     | Sevcon 0x4600: Additional Motor Measurements                                                                             | Subindex 0 - 27                                                                                         |
| 10100-10109     | Sevcon 0x6C01: Analog input values.                                                                                      | Subindex 0 - 8                                                                                          |
| 10110-10111     | Sevcon 0x2620                                                                                                            | 2 rekordy                                                                                               |
| 10115-10119     | Sevcon 0x29A0: Odometer Counter                                                                                          | Subindex 0 - 3                                                                                          |

### Obowiązujące id danych telemetrycznych `double` na dzień 21-05-2024
| Identyfikator (dec) | Opis                       | Komentarz                                 |
| ------------------- | -------------------------- | ----------------------------------------- |
| 100                 | Prędkość przedniego koła   |                                           |
| 101                 | Prędkość średnia   		   |                                           |
| 110                 | Ugięcie amortyzatora przód |                                           |
| 111                 | Ugięcie amortyzatora tył   |                                           |
| 120                 | TPMS 1                     | Todo                                      |
| 121                 | TPMS 2                     | Todo                                      |
| 200                 | Temperatura baterii        | Ale do poprawy bo niezgodne z wyżej, sorry|
| 201                 | Temperatura cieczy         | 							               |
| 202-203             | Temperatury                | Jeszcze nwm czego ale z stm-ki            |
| 200-203             | Temperatury                | Jeszcze nwm czego ale z stm-ki            |
| 1000          	  | Napięcie ogniwa            | Ustawione w config.json                   |
| 1001-1999           | Napięcia ogniw             |                                           |
| 3000                | Naładowanie baterii        |                                           |
| 3001                | Rotacja motocykla          | Ustawione tymczasowo, do poprawy          |
| 10009               | Heat sink temperature      |                                           |
| 10012               | Torque actual              |                                           |
| 10017               | Bat voltage                | Dokładnie to napięcie zasilające sevcon-a |
| 10018               | Cap voltage                |                                           |
| 10101               | Analog input 1             | 1 to odkręcenie manety                    |
| 10073               | Motor temp                 | Temp silnika                              |
| 10082               | AC motor current           |                                           |
| 10083               | AC motor voltage           |                                           |
| 10110               | Throttle value             |                                           |
| 10116               | Odometer km                |                                           |

### Obowiązujące id danych telemetrycznych `location` na dzień 21-05-2024

| Identyfikator (dec) | Opis              | Komentarz |
| ------------------- | ----------------- | --------- |
| 1                   | Pozycja motocykla |           |
