Aplikacja parsująca surowe ramki [[can]]-a i zapisujące je w [[mqtt]]

https://gitlab.com/spectrumagh/e-moto/can-to-mqtt

---
Aplikacja wymaga socket-u (w terminologii linux-a)

Za pomocą [[konfiguracja can2mqtt]] wskazuje się jak odczytać ramkę can. Tak jak w tym opisie parametrów [[identyfikatory telemetrii]]

Aplikacja wspiera przypadki:
- Parsowania przychodzącej ramki
- Wykonywania zapytań, odpytujących urządzenia o ramki can (model klient-server)
	- w szczególności [[Komunikacja Sevcon CAN]], gdzie trzeba wysyłać ramki pytające

