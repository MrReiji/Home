Serwer [[mqtt|MQTT]]

## Instalacja

```bash
sudo apt install mosquitto
```

Lub [[mosquitto docker]]

## Otwarcie portów i anonimowy użytkownik

Wtedy [[mqtt]] nasłuchuje tylko na localhost, aby nasłuchiwał na wszystkich portach można zmienić to w konfiguracji w `/etc/mosquitto/mosquitto.conf` dopisując

```ini
port 1883
allow_anonymous true
```

Wtedy poniższe plecenie pokaże nasłuchiwanie na wszystkie adresy
```bash
ss -tunlp | grep 1883
```