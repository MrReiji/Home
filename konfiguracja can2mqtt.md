Rozróżniane są dwie metody konfiguracji przychodzących ramek:
- surowa - dokładna
- uproszczona dla standardu CANOpen SDO
	- program parsuje ostatecznie do surowej postaci

Natomiast CANOpen SDO to model klient serwer, a więc serwer odpytuje urządzenia CAN, i ta funkcjonalność również jest dostępna.

Konfiguracje odbywają się w plikach [[yaml]]. Aplikacja na dzień 25.05.2024 czyta wszystkie pliki w folderze `./conf.d/*.yaml` przez co można sobie odpowiednio ponazywać

---

Przykład dokładnego zapisu:
```yaml
Can:
  - topic: 1001
    frameId: 0x666
	length: 2
    offset: 1
    scale: 0.25
    reversed: true
    data:
      filterLen: 0
	  offset: 0
      filter: [0x01]
```

Aby wskazać że będziemy mieli doczynienia z dokładną konfiguracją głównym nodem jest zapis `Can:`
Obiekt ten zawiera tablice z konfiguracją z której to jeden z elementów zawiera poniższe informacje: 
- `topic` - suffix topic-a mqtt na który to trafiają przetworzone ramki zgodnie z [[identyfikatory telemetrii]]. Parametr opcjonalny, gdy jego brak nie nastąpi wysyłka
- `frameId` - identyfikator ramki CAN. Parametr opcjonalny, gdy jego brak nie nastąpi wysyłka
- `length`- długość ramki. Mówi z ilu bajtów ramki can składa się informacja. Domyślnie 1
- `offset` - pozycja pierwszego bajtu informacji. Wskazuje od którego bajtu należy odczytywać wartości. Domyślnie 0
- `scale` - Mnożnik wartości. Wartość przez którą zostanie przemnożona przetworzona wartość. Domyślnie 1.0
- `reversed` Odwrotny odczyt danych. Coś w stylu: MSB/LSB, little / big endian
	- Dla przykładu: zał. ramkę która ma początkowe wartości: `[0x01 0x02]` oraz `length` == 2
	- Gdy`reversed` ustawione na fałsz przetworzona wartość to `0x0102`
	- Gdy`reversed` ustawione na true przetworzona wartość to `0x0201`
	- Domyślnie fałsz
- Obiekt `data` - służy on głównie do filtrowanie zawartości ramki CAN, używane przy BMS-ie oraz CANOpen SDO (w jednym id ramki can są wszystkie możliwe informacje). Domyślnie jest pomijany. Use case: [[Komunikacja Sevcon CAN]]
	- `offset` - index pierwszego bajtu w danych na który nałożony będzie poniższy filtr. Domyślnie 0
	- `filter` - tablica wartości, w której to dane w przetwarzanej ramce can muszą być równe tym danym co są w filtrze
	- Przykładowo w [[sevcon]] interesuje nas konkretne ramka, a w niej konkretne wartość na 1, 2 (index) oraz 3 bajcie (subindex), 0 bajt nie jest istotny. A treści zaczynają się od 4 bajta

Ze względu na wartości domyślne minimalna konfiguracja pozwalająca na wysyłkę na [[mqtt]], może wyglądać tak jak poniżej:

```yaml
Can:
  - topic: 100 # kanał mqtt
    frameId: 0x111 # id ramki can
```

---

W tym też formacie możliwa jest konfiguracja zapytań w modelu klient-server. Przydatne dla konfiguracji BMS-a, oraz CANOpen SDO,  tam należy odpytywać go o dane:

```yaml
Can:
  - req:
      ms: 250
      msg: []
      frameId: 0x18950140
```
Konfiguracja:
- `req` Obiekt w którym jest zawarta konfiguracja. Domyślnie null, tj. pomijane
	- `ms` czas w milisekundach, które generują ramki can
	- `msq` tablica z danymi, które mają się pojawić w ramce pytającej
	- `frameId` identyfikator ramki can

---
CANOpen: SDO jest standardem, po przeczytaniu czym jest wspomniany "profil" SDO można dojść do wniosku że powyższa metoda zapisu byłaby dosyć rozwlekła, a więc jest dostępny jest uproszczony model konfiguracji. Zakładam podstawowe kompetencje co do CANOpen SDO. Przykład został przedstawiony tu: [[Komunikacja Sevcon CAN]]. Przykład konfiguracji poniżej:

```yaml
CanOpenSDO:
  senderMeta: 0x40
  nodeId: 1
  reversed: true
  items:
    - index: 0x4602
      items:
        - subindex: 17
          req:
            ms: 250
          topic: 10017
          offset: 0 # offest + 4 (sdo) 
          length: 2
          scale: 0.0625

```

`CanOpenSDO` Główny obiekt wskazujący na tryb uproszczony
- `senderMeta` Pierwszy bajt w ramce, która wysyła zapytania o dane
- `nodeId` Identyfikator `NodeId`. Na jej podstawie obliczane są id ramek can
- `reversed` jw. (Analogicznie jak dla przypadku dokładnej konfiguracji)
- `items` Tablica konfiguracji dla urządzenia o tym `NodeId`
	- `index` Index w terminologii CANOpen
	- `items` Tablica konfiguracyjna dla wybranego index-u (często zawiera dużo subindex-ów)
		- `subindex` Subindex w terminologii CANOpen
		- `req` jw. Ale..
			- `ms` - jw
			- `frameId` Identyfikator ramki CAN pytającej jest wg. standardu można obliczyć na podstawie `nodeId`, a więc tu opcjonalne
			- `msg` Zawartość ramki tworzona na podstawie standardu: 
				- `[ senderMeta index_2 index_1 subindex 0x00 0x00 0x00 0x00 ]`
		- `topic` jw
		- `offset`. jw ale przesunięty o wartość 4. Ponieważ standard CANOpen SDO zakłada że pierwsze 4 bajty zostaną przeznaczone na "meta dane" (indexy itp), pierwszy możliwy bajt danych zaczyna się od 4 pozycji. A więc offest ustawione na zero wskazuje pierwszy możliwy bajt z informacją tj. 4
		- `length` jw
		- `scale` jw
