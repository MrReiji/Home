# Jak sklonować to repozytorium

Ponieważ repozytorium posiada submoduły, dla róznych poziomów wtajemniczenia klonuje się je rekursywnie tj. z submodułami
```bash
git clone --recurse-submodules git@gitlab.com:spectrumagh/documentation/Home.git
```
Przydatna opcja po sklonowaniu

```bash
git config submodule..obsidian.ignore all
```

# Jak używać te repozytorium

- Obsidian (zalecane)
- Bezpośrednio w przeglądarce

# Uruchomienie Obsidiana

1. Pobierz [Obsidiana ze strony](https://obsidian.md/)
2. Sklonuj niniejsze repozytorium
3. Otwórz `Valut` (storage) Obsidiana wskazując mu lokalizację pobranego repozytorium
