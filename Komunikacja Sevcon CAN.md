Sevcon wspiera aplikacyjnie CANOpen https://en.wikipedia.org/wiki/CANopen

Domyślnie na prędkości CAN 1Mbps

W tym przypadku id node sevcon-a to 1, biorąc pod uwagę ramki `Communication objects` typu `SDO` adresy ramek są następujące (z punktu widzenia sevcon-a):
- wysyłająca 581h
- odbierająca 601h

A więc wysyłamy żądania na 601h a oczekujemy na odpowiedź pod adresem 581h

W odbiorczej ramce na podstawie dokumentacji producenta sevcona pierwsze bajty oznaczają
- 0 - jakaś metadata
- 1, 2 - czytany od tyłu - identyfikator parametru (ten z excela)
- 3 - podindex - subindex (ten z excela)
- 4, 5, 6, 7 - dane (najcześciej przeskalowane wg informacji)

```
4B 02 46 11 2D 02 00 00
```


Zapytanie o dane jest analogiczna jak wyżej, lecz ramka z danymi jest wypełniona zerami

```
4B 02 46 11 00 00 00 00
```

### Przykład

Wyciąg z dokumentacji dla temperatury baterii

| **Index** | **Sub-Index** | The top level object has the same version as subindex 0. i.e. 1010h has the same version as 1010h,0. **Version** | RO - Read Only WO - Write Only RW - Read / Write RWR - RW (mappable to TPDO) RWW - RW (mappable to RPDO) **Access Type** | **Access Level** | **Scaling** | **Units** | **Name**        | **Data Type** | Can be decimal or hex (append with 'h') or floats (Data Type must be Real32). **Low Limit** | Can be decimal or hex (append with 'h') or floats (Data Type must be Real32). **High Limit** | Values can be decimal or hex (append with 'h') or floats (Data Type must be Real32). When protected, only cells containing configuration parameters can be edited. **Value** | **Description**          | **Map to PDO?** | Bit 0 = Inhibit write during download. Bit 1 = Inhibit read during upload. **Object Flags** |
| --------- | ------------- | ---------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------ | ---------------- | ----------- | --------- | --------------- | ------------- | ------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------ | --------------- | ------------------------------------------------------------------------------------------- |
| 4602h     | 17            | 1                                                                                                                | RO                                                                                                                       | N/A              | 0.0625      | V         | Battery Voltage | Unsigned16    | 0                                                                                           | 65535                                                                                        | No                                                                                                                                                                           | Battery voltage in 12.4V | Yes             | 0                                                                                           |

Najpierw pytam o pomiar, używając socket can-a dla can0, a więc:

```bash
cansend can0 601#4002461100000000
```

Payload to kolejno:
- 0 - jakaś metadata 40
- 1, 2 - index (1 kolumna) ale notowany od tyłu więc 46 02 (hex) zamienia się na: 02 46 (hex)
- 3 - subindex (2 kolumna) mamy zapisane w decymalnym systemie 17, więc jest to 11h w hex
- reszta zer

Nasłuchując w tle (znak `&`) na przychodzące ramki
```bash
candump can0,581:581 &
```

Otrzymuje:
```
can0  581   [8]  4B 02 46 11 2D 02 00 00
```

Pierwsze 4 bajty są analogiczne.  Z kolumny `Data type` mam informację że to `Unsigned16` a więc dwa bajty.
Odczytuje w dalszej kolejności następne dwa bajty `2D 02`, odwracając mam `022D (hex)` tj `557 (decymalnie)` mnożąc to przez wartość z kolumny `Scaling` mam `557 * 0,0625 = 34,8125` tj ok. 34,81\[V\]