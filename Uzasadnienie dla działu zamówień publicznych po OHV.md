
Koło Naukowe Przetwarzania Sygnałów "Spectrum" z Wydziału Informatyki Elektroniki i Telekomunikacji (Instytut Elektroniki) planuje płatną subskrypcję na usługi chmury obliczeniowej. Są one niezbędne do realizacji innowacyjnego projektu, odpowiedzialnego za gromadzenie i  agregację danych telemetrycznych w czasie rzeczywistym. Dane te są informacją zwrotną niezbędną do rozwijania motocykli powstających w ramach projektu E-Moto AGH, konkurujących na międzynarodowych zawodach. Jest to jedyny taki projekt studencki w Polsce.

Część projektu - aplikacja internetowa została już wyróżniona w pierwszej edycji festiwalu IT is ME. Implementacja rozwiązania zakłada wdrożenie aplikacji mikroserwisowych na maszynach obliczeniowych dostępnych publicznie. Mikroserwisy będą wdrażane na wysoko dostępny klaster kubernetes. Wspomniane publiczne maszyny / zasoby chmurowe umożliwiają nam możliwość wysyłania danych telemetrycznych z motocykli oraz pobrania jej dostosowanej ilości przez dowolnych użytkowników.

Analizując inne rozwiązania, żadne nie daje nam publicznie dostępnych maszyn obliczeniowych, poza usługodawcami chmurowymi. A dostawca OVH na dzień dzisiejszy oferuje najbardziej atrakcyjnie cenowo rozwiązanie, gdzie koszt subskrypcji na dwa lata wynosi 1 377,87 zł netto.

Oferty (najlepiej dedykowany eco)
- VPS typu "Comfort": https://www.ovhcloud.com/pl/vps/ 
	- 24 miesiące z góry: 
		- 1 996,14 zł netto
		- 2 455,25 zł brutto
	- 12 miesięcy z góry:
		- 1 080,26 zł netto
		- 1 328,72 zł brutto
	- 4xCpu, 8 Gb RAM, 160GB Nvme, 1Gb/s net
- Serwer dedykowany "Eco: So you Start: SYS-4-SAT-16": https://eco.ovhcloud.com/pl/soyoustart/
	- 12 miesięcy z góry:
		- 1 377,87 zł netto,
		- 1 694,78 zł brutto
	- 8xCpu, 16Gb RAM, 4TB HDD, 0,25 Gb/s net
	- CPU: Intel Xeon E3-1230v6 - 4c/8t - 3.5 GHz/3.9 GHz
	