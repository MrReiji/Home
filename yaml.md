YAML Ain't Markup Language ([YAML](https://yaml.org/)) is a data serialization language that is consistently listed as one of the most popular programming languages. It's often used as a format for configuration files, but its object serialization abilities make it a viable replacement for languages like JSON


https://www.cloudbees.com/blog/yaml-tutorial-everything-you-need-get-started