Aplikacja przesyłająca dane z serwera [[mqtt]] do [[RabbitMQ]]
Aplikacja do metryki dodaje czas pojawienia się mierzonej wartości oraz:
- Id maszyny - motocykla
- Typ mierzonej metryki z tematu [[mqtt]]. Zazwyczaj [[identyfikatory telemetrii]]

Aplikacja wspiera autoryzację [[jwt]]

[Repozytorium](https://gitlab.com/spectrumagh/webappteam/datastream-WebApp/-/tree/master/Mqtt2RabbitMQ?ref_type=heads)