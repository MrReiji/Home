Zaczynając z C# w WebApp, zaczynamy oczywiście od C#, ale dla aplikacji internetowej zazwyczaj pojawia się warstwa danych np. baza danych. Do niej nie pisze się bezpośrednio zapytań SQL ze zmiennej testowej, tylko używa się bibliotek ORM np. EntityFramework.

Współcześnie zagłębiając się w C# polecam playliste Nicka-a, dosyć już stara jednakże dużo wyjaśnia ciekawych knceptów: https://www.youtube.com/watch?v=sdlt3-ptt9g&list=PLUOequmGnXxOgmSDWU7Tl6iQTsOtyjtwU

Ogólnie polecam popatrzyć na jego koncepty, pooglądać, podczas zagłębianie się w .dotnet-a:  https://www.youtube.com/@nickchapsas/featured

