## K8

O ile docker-compose pozwala na uruchamianie serwisów w kilku kontenerach połączonych siecią, to odbywa się to na jednym komputerze. Dlatego używamy kubernetes-a, aby móc sobie ubić kontener, czy cały komputer, aby ruch był balansowany między różne aplikacje, aby wersjonować aplikacje, tworzyć różne środowiska: prod, dev, review itp itd.

Do obiadka: https://www.youtube.com/watch?v=7bA0gTroJjw

Na początek szczególnie co to jest https://kubebyexample.com/learning-paths/kubernetes-fundamentals/what-kubernetes-3-minutes a potem reszta z kursiku na 

https://kubebyexample.com/ a wg. zaleceń warto zacząć od `Kubernetes fundamentals`

Podsumowanie architektury k8s: https://www.youtube.com/watch?v=TlHvYWVUZyc

Kurs na yt: https://www.youtube.com/watch?v=X48VuDVv0do a w nim:
- Co to [[kubernetes]]
- Architektura k8s
- lokalny [[kubernetes]] - [[minikube]]
- [[kubectl]]
- kubernetes [[yaml]]
- demo
- namespace (nie linux namespaces)
- ingress
- helm
- volumes
- StatefulSet
- Services
W skrócie "dosłownie" wszystko jest używane w projekcie

## Ingress controller

Często różne narzędzia domyślnie używają serwisu typu `LoadBalancer`, o ile w chmurze google to zadziała, to w przypadku własnej implementacji klastra, trzeba o to zadbać samemu. Jest rozwiązanie takie jak [MetalLB](https://metallb.universe.tf/) ale nie zadziała ono na chmurze Oracle, (dodatkowe protokoły routingu + nie jest to w ich interesie). Można to tematu podejść z wykorzystaniem publicznego adresu ip maszyny, tj zasobu typu [ingress](https://kubernetes.io/docs/concepts/services-networking/ingress/). Tp Abstrakcja działające w warstwie L7 Http(s) 

Abstrakcję Ingress-a należy zaimplementować, np. przy pomocy kontrolera [NGINX Ingress Controller](https://docs.nginx.com/nginx-ingress-controller/overview/design/) Dzięki temu mamy routing http, używamy jednego adresu ip, i udostępniamy różne serwisy

## Helm

Wraz ze wzrostem skomplikowania aplikacji, serwisów, service mesh-a rośnie problem konfigurowania ich wszystkich. Analogicznie jak menadżery paczek `apt`, `dnf/yum`, `winget` zarządzają paczkami na systemie, helm zarządza manifestami kubernetesa. 

https://www.youtube.com/watch?v=-ykwb1d0DXU

## Kustomize

Co jeśli chcemy dynamicznie, bez wydawania nowej wersji helm-owej zaktualizować wersję aplikacji. Zaktualizujemy najczęściej tag obrazu we wdrożeniu, więc jest to pewna zmienna. Wdrożenia można tak uzmienniać w stylu szablonów i jednym z takich narzędzi jest [kustomize](https://kustomize.io/) który to umożliwia z CI/CD. Scala on szablony ze zmiennymi - taki merger. Ciekawym zastosowaniem jest też dodawanie `label`-ek, prefixów tworząc w ten sposób środowisko produkcyjne, deweloperskie, UAT.

https://www.youtube.com/watch?v=ASK6p2r-Yrk


