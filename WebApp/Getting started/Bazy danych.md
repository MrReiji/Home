W projekcie używana jest baza [PostgreSQL](https://www.postgresql.org/) W przeciwieństwie do języków składnia baz danych typu SQL jest ustandaryzowana, pojawiają się różnice jednakże jest to język w miarę uniwersalny w kontekście relacyjnych baz danych

Polecam książkę dosyć obszerną: https://helion.pl/ksiazki/praktyczny-kurs-sql-wydanie-iii-danuta-mendrala-marcin-szeliga,pksq3v.htm

Dla problemu pomiarów w domenie czasu pojawia się problem szeregów czasowych, a więc i wydajności, a na to rozwiązaniem jest [TimescaleDB](https://www.timescale.com/), rozszerzenie do PostgreSQL-a które przyspiesza przetwarzanie zapytań.

Ponadto dla kartografii używane jest jeszcze rozszerzenie [PostGIS](https://postgis.net/) które posiada dużo funkcjonalności specyficznych dla danych przestrzennych.
