Aplikacja ogólnie rozwijana z użyciem biblioteki [React](https://react.dev/)  z którą używamy [TypeScript](https://www.typescriptlang.org/)-a. Zarówno React jak I TypeScript (TS) pod sobą wykorzystuje JavaScript-a (JS). Z tego powodu zaczynając z React-em warto poświęcić czas na rozwój umiejętności JS-a.
Zaletą (dla innych wadą) TS-a jest statyczne typowanie (jak w c++/java) zmiennych.
TS jest rozszerzeniem JS-a z wymogiem wspomnianego typowania, także ucząc się TS-a automatycznie pogłębiamy wiedzę z JS-a

TS to taka nakładka na JS-a, która rozszerza syntax języka, ale z niego nie rezygnuje. Przy budowaniu kodu napisanego w React przy użyciu TS/JS mamy do czynienia z transpilacją kodu, czyli zamiany z JS-a na JS-a (skompresowanego). Kompilacja występuje np. w c++ gdzie przechodzimy z C++ na Asemblera, a potem kodu maszynowego

Poniższa książka jest o tyle ciekawa że początek jej jest poświęcony JS-owi z punktu widzenia React-a oraz Reactowi https://www.oreilly.com/library/view/learning-react-2nd/9781492051718/
- JS dla Reacta
- Czyste funkcje
- JSX
- useState, useEffect, useContext, useReducer, useMemo, useCallback
- fetch, oraz własny hook: useFetch
- suspense
- ESlint, VSCode, TS, TDD, unittesty
- React router
- React serwer side

Strukturyzacja aplikacji bywało wyzwaniem szczególnie na początku. Natomiast przyjęto zasadę z tego nagrania 
https://www.youtube.com/watch?v=UUga4-z7b6s

Ogólnie ten autor ma sporo ciekawostek apropo frontendu, Reacta: i ogólniej fullstack-u:
https://www.youtube.com/@WebDevSimplified