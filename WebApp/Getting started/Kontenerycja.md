Skrótowo:
Docker-compose - przepis na uruchomienie m.in kontenerów docker-a
Docker - Narzędzie ułatwiające konteneryzację, budowanie obrazów, udostępniające rejestry obrazów. Do konteneryzacji używa np. ContainerD
ContainerD - Narzędzie na niższym poziomie zajmującym się tylko konteneryzacją, wykorzystuje runc
runc - proces userspace konfigurujący namespace (funkcjonalność jądra systemu linux).  Istnieje kilka typów namespace izolujące między sobą: sieci, procesy, pamięci masowe itp. runc też konfiguruje cgrupy - tj limity zasobów komputera na proces

---

Ogólnie [[docker]]

Godny uwagi pierwszy film https://www.youtube.com/watch?v=pg19Z8LL06w który zwiera:
- po co docker, świat przed kontenerami
- instalowanie
- rejestry [[docker]]
- tworzenie obrazu
- komendy docker-a
- wersjonowanie obrazu

Podobnie jak z oficjalnej strony, lecz mniej obszernie: https://docs.docker.com/guides/get-started/
Konteneryzacja dla specyficznych języków: https://docs.docker.com/language/
	Jest tam używany C#, Node.js

Ściągawka docker: https://dockerlabs.collabnix.com/docker/cheatsheet/
- docker cli
- dockerfile
- docker scout

Po zapoznaniu się z powyższymi materiałami, wiemy o montowaniu dysków, publikowaniu portów, kart sieciowych. No ale te polecenie staje się dość długie. Ponadto jak frontend, wymaga backendu, który wymaga bazy danych, uruchamianie tych kontenerów może być problematyczne co zostało rozwiązanie przez docker-comopse (do jednej komendy): https://www.youtube.com/watch?v=DM65_JyGxCo


Jednakże Docker, oraz Docker-In-Docker (kontener na obrazie Dockera), wymaga przekazania/zamontowania socket-a(linux socket), co nakłada wymóg aby hosty z kubernetesem miały zainstalowanego docker-a. Dlatego jest narzędzie [[kaniko]] które nie ma takiego wymogu - jest projektem co stworzy obraz docker-a, bez zainstalowanego docker-a. Więcej: https://docs.gitlab.com/ee/ci/docker/using_kaniko.html

Zaawansowane, lecz pokazujące jak ta konteneryzacja jest realizowana: linux namespaces https://www.nginx.com/blog/what-are-namespaces-cgroups-how-do-they-work/
Jednym z typów namespace to network namespace - ideowo to prosta wirtualna karta sieciowa: https://ramesh-sahoo.medium.com/linux-network-namespace-and-five-use-cases-using-various-methods-f45b1ec5db8f i ciekawostka odemnie: jeśli nie opublikuje się portów można zrobić zapytanie pod ip kontenera, przez tą właśnie wirtualną kartę sieciową

