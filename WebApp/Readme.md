# Cel aplikacji E-Moto AGH

Aplikacja ma rozwiązywać problem łatwej wizualizacji telemetrii przesyłanej z pojazdów.

W zależności od uprawnień nadanych przez administratora pojazdu dane będą udostępniane  odpowiednim osobą.

Odbiorca będzie mógl je procesować przetwarzać, agregować, wizualizować w wybrany przez siebie sposób.

# Linki do aplikacji


| Wersja                           | Adres       | Częstotliwość zmian | Status CI/CD |
|:-----------------------------------------:|:-----------------------------------------:|:-------------------:|:---:|
| Wersja stablina                           | https://emotoagh.eu.org/        | Rzadkie aktualizcje | [![pipeline status](https://gitlab.com/spectrumagh/webappteam/deployments-WebApp/badges/master/pipeline.svg)](https://gitlab.com/spectrumagh/webappteam/deployments-WebApp/-/commits/master)
| Wersja developerska - build produkcyjny   | https://dev.emotoagh.eu.org/       | Częste aktualizacje |[![pipeline status](https://gitlab.com/spectrumagh/webappteam/deployments-WebApp/badges/dev/pipeline.svg)](https://gitlab.com/spectrumagh/webappteam/deployments-WebApp/-/commits/dev)


# Przydatne publiczne linki

|                     | Nazwa       | Adres       | Funkcja |
|:-------------------:|:-----------:|:-----------:|:-------------------:|
| [<img src="https://upload.wikimedia.org/wikipedia/commons/3/33/Figma-logo.svg" height="24"/>](https://www.figma.com)| Figma | [Projekt figma](https://www.figma.com/files/project/66505412/Team-project?fuid=1150829018930982986) | Prototyp oraz notatki projektu |
| [<img src="https://upload.wikimedia.org/wikipedia/commons/f/f5/OpenVPN_logo.svg" height="24"/>](https://openvpn.net/)| OpenVpn | emotoagh.eu.org | Serwer VPN ograniczający dostęp do wrażliwych danych |

# Przydatne prywatne linki
Dostępne poprzez VPN

|                     | Nazwa       | Adres       | Funkcja |
|:-------------------:|:-----------:|:-----------:|:-------------------:|
| [<img src="https://upload.wikimedia.org/wikipedia/commons/4/4e/Docker_%28container_engine%29_logo.svg" height="24"/>](https://www.docker.com/)| Docker Registry | [App docker registry](https://registry.emotoagh.eu.org/v2/_catalog) | Rejestr obrazów dockerowych aplikacji |
|                     | Docker Registry UI | [Docker Registry UI](https://registry-ui.emotoagh.eu.org/) | Widok rejestru obrazów dockerowych aplikacji
| [<img src="https://upload.wikimedia.org/wikipedia/commons/3/39/Kubernetes_logo_without_workmark.svg" height="24"/>](https://kubernetes.io/) | Kubernetes| kubectl proxy | Panel zarządzania własnym klastrem kubernetes
| [<img src="https://grafana.com/oss/assets/logo-grafana-spiral.svg" height="24"/>](https://grafana.com/) | Grafana | [Grafana dashboards](https://grafana.emotoagh.eu.org/) | Monitorowanie 

### See also: 

- [[Stack technologiczny aplikacji webowej]]
- [[Wdrożenie rozwiązania aplikacji webowej.canvas|Wdrożenie rozwiązania aplikacji webowej]]