Łączenie ról w role

tricky ustawienie
https://github.com/keycloak/keycloak-community/blob/main/design/admin-console/RealmRoles/composite-role.md


Flow-y: https://developer.okta.com/docs/concepts/oauth-openid/


https://www.appsdeveloperblog.com/keycloak-client-credentials-grant-example/


ok2
view-realm

Dla zapytań: https://iam.emotoagh.eu.org/admin/realms/app/roles/hmmm/composites
potrzebna jest poniższa rola:
```
manage-realm
```

## Klient dla aplikacji backend - bez interakcji użytkownika

OpenId flow: client-credentials

Dla odpowiedzieniego `realm` należy otworzyć zakładkę `Clients` a w niej `Create client`

#### General Settings

Wpisanie `ClientId` tj. nazwy "urządzenia" (nie człowieka)

![[Pasted image 20240203184237.png]]

### Capability config

Zaznaczenie `Client authentication`, ponieważ ten typ flow jest interesujący w tym przypadku
Zaznaczenie `Service accounts roles` aby mieć możliwość uzyskania tokena JWT.

![[Pasted image 20240203184326.png]]

### Login settings - domyślnie

W utworzonym kliencie w zakładce `Credentials` dostępne są opcje uwierzytelniające. Istotne jest tutaj pole `Client secret`. Początkowo należy tutaj wygenerować hasło, wykorzystywane później. (zdjęcie przed wygenerowaniem)

![[Pasted image 20240203184630.png]]

### Test uzyskania `Access token`

```bash
curl --location --request POST 'https://iam.emotoagh.eu.org/realms/app/protocol/openid-connect/token' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'client_id=<client_id>' \
--data-urlencode 'client_secret=<client_secret>' \
--data-urlencode 'grant_type=client_credentials'
```

W odpowiedzi json powinien się pojawić `access_token`. Jego sprawdzenia można dokonać na stronie https://jwt.io/

# Odczyt ról z [Keycloak Admin REST API](https://www.keycloak.org/docs-api/22.0.1/rest-api/index.html#_roles)

W kliencie należy dodać rolę `view-realm`. Przechodząc do zakładki `Service accounts roles` w kliencie

![[Pasted image 20240203185942.png]]

# Dodanie motocykla z uwierzytelnianiem KeyCloak

Podobnie jak wyżej należy stworzyć klienta z flow client_credentials

## Jako testowy klient posłużę się poniższym przykładem

```python
import datetime, time, pika, requests

client_id = ''
client_secret = ''
token_lifespan = 50


def new_access_token():
    r = requests.post('https://iam.emotoagh.eu.org/realms/app/protocol/openid-connect/token',
        headers={'Content-Type': 'application/x-www-form-urlencoded'},
        data={'client_id': client_id, 'client_secret': client_secret, 'grant_type': 'client_credentials'})

    return r.json()["access_token"]

last_update = datetime.datetime.now()
def check_for_update_access_token(connection: pika.BlockingConnection, token_lifespan: float):
    global last_update
    now = datetime.datetime.now()
    if (now - last_update).total_seconds() > 50:
        connection.update_secret(new_access_token(), 'secret')
        print('Updated JWT after {} [s]'.format((datetime.datetime.now() - last_update).total_seconds()))
        last_update = now


credentials = pika.PlainCredentials(username='', password=new_access_token())

connection = pika.BlockingConnection(pika.ConnectionParameters(
    virtual_host="/",
    credentials=credentials))

main_channel = connection.channel()

tnow = datetime.datetime.now()
last_update = datetime.datetime.now()

for i in range(0, 100):
    msg = 'MyMessage {}'.format(i)
    print('Sending message: {}'.format(msg))

    check_for_update_access_token(connection, token_lifespan)
    
    main_channel.basic_publish(
        exchange='',
        routing_key='test',
        body=msg,
        properties=pika.BasicProperties(content_type='application/json'))
    time.sleep(5)

connection.close()
```
Źródło: https://github.com/rabbitmq/rabbitmq-oauth2-tutorial/blob/main/pika-client/producer.py
## Konfiguracja rabbitmq

Źródło: https://www.rabbitmq.com/oauth2.html

W pliku conf istotne są następujące rzeczy:
```ini
auth_backends.1 = rabbit_auth_backend_oauth2
auth_oauth2.resource_server_id = account
auth_oauth2.jwks_url = https://iam.emotoagh.eu.org/realms/app/protocol/openid-connect/certs
```
## Konfiguracje KeyCloak

1. W JWT ważne jest pole `aud` ono **musi** pokrywać się z `resource_server_id` w konfiguracji rabbit-a
2. W tokenie istotny jest **scope** w stylu `<resource_server_id>.<read/write>.<prawa>` dla powyższego przykładu zawarty jest scope: `account.write:*/*`