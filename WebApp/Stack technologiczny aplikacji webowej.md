- [React](https://reactjs.org/)
- [.NET](https://dotnet.microsoft.com/en-us/)
- [PostgreSQL](https://www.postgresql.org/)
- [Oracle OCI](https://www.oracle.com/cloud/)
- [Gitlab Runner](https://docs.gitlab.com/runner/)
- [nic.eu.org](https://nic.eu.org/)
- [Hostry](https://hostry.com/)
- [Terraform](https://www.terraform.io/)
- [Ansible](https://www.ansible.com/)